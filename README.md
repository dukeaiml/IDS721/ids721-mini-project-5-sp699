# IDS-721-Cloud-Computing :computer:

## Mini Project 5 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Create a Rust AWS Lambda function (or app runner)
* Implement a simple service
* Connect to a database

## :ballot_box_with_check: Grading Criteria
* __Rust Lambda functionality__ 30%
* __Database integration__ 30%
* __Service implementation__ 30%
* __Documentation__ 10%

## :ballot_box_with_check: Deliverables
* Rust code
* Screenshots or demo video showing successful invocation
* Writeup explaining service

## :ballot_box_with_check: Main Progress
1. __`Create the Rust function`__: Create a new directory with Cargo Lambda and create the Rust function. In this project, the Rust function reads the database and then responds with the average value of the scores from the Math and Cloud classes through DynamoDB.
```rust
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use lambda_runtime::{Error as LambdaError, LambdaEvent, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use simple_logger::SimpleLogger;

#[derive(Deserialize)]
struct Request {
    id: String,
}

#[derive(Serialize)]
struct Response {
    average: f32,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new().with_utc_timestamps().init()?;
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Request>) -> Result<Value, LambdaError> {
    let request = event.payload;
    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = get_scores_average(&client, request.id).await?;

    Ok(json!({ "average": response.average }))
}

async fn get_scores_average(client: &Client, id: String) -> Result<Response, LambdaError> {
    let table_name = "StudentScores";

    let resp = client.get_item()
        .table_name(table_name)
        .key("id", AttributeValue::S(id))
        .send()
        .await?;

    if let Some(item) = resp.item() {
        let math_score: i32 = item.get("math").and_then(|v| v.as_n().ok()).and_then(|n| n.parse().ok()).unwrap_or(0);
        let cloud_score: i32 = item.get("cloud").and_then(|v| v.as_n().ok()).and_then(|n| n.parse().ok()).unwrap_or(0);
        let average = (math_score + cloud_score) as f32 / 2.0;

        Ok(Response { average })
    } else {
        Err(LambdaError::from("No data found for the provided ID"))
    }
}
```

2. __`Build and Deploy the function to AWS Lambda`__: After creating `main.rs` file, build and deploy the function to AWS.
```bash
$ cargo lambda build --release
$ cargo lambda deploy
```

![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/5eaf804b-419a-4029-95db-8b59fc95e187)

- Successfully built and deployed.

3. __`Create the DynamoDB Table`__: Create the database using DynamoDB. The table name should be matched with the rust function.

(1) Insert a few items into the DynamoDB table, in this case, named 'StudentScores'.
![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/5edf7bd1-3bb1-43d1-a79f-4995ae3e4f53)

(2) Attach the policy to Lambda instance.
- Click on the AWS Lambda function.
- Navigate to `Configuration` -> `Permission`.
- Click on the link labeled Role name.
- Attach the policy you intend to use. For this project, the __`GetItem`__ role has been added.
```json
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "VisualEditor0",
			"Effect": "Allow",
			"Action": "dynamodb:GetItem",
			"Resource": "arn:aws:dynamodb:[region]:[AWS ACCOUNT ID]:table/[DynamoDB Table Name]"
		}
	]
}
```

4. __`Test the function`__: Test the function to see if it connects with the database.

(1) Test-1
![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/1289b956-92de-4024-8d97-4dcaa6b0e2a3)
![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/e12447dd-c57d-4863-8e23-3f7ba8b884be)

(2) Test-2
![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/e314a30b-8056-45a7-a7f9-7bd06da3f477)
![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/fd092b90-9d8a-41fb-a9ec-9b677cf75410)

(3) Test-3
![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/bcd2eec0-9576-4c99-bae4-4820ff17e9d3)
![image](https://github.com/suim-park/Mini-Project-1/assets/143478016/236a0576-109c-472f-a9de-807ccbffdf42)

* The database has been successfully connected to the Lambda function.
